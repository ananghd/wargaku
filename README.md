### Wargaku

Aplikasi ini digunakan untuk management lingkungan.
Mulai dari tahap yg paling kecil keluarga, sampai ke sistem smart city.
Dan diharapkan bisa digunakan untuk fasilitas seluruh indonesia.

### Features
- Manajemen User
- Pengumuman / Broadcast Informasi
- IPL - Iuran Pengelolaan Lingkungan


### Future
- MANAJEMEN RT / RW / CLUSTER / PERUMAHAN / APARTEMEN
- LAPOR DAN TOMBOL PANIK (Pengaduan)
- NOTIFIKASI PENGINGAT (Reminder) 24 Jam
- PAYROLL - Pembayaran Gaji keamanan dan kebersihan
- CCTV Terintegrasi
- CHAT BOT
- Payment Gateway

#### System Requirement
- Nodejs
- ExpressJs
- mySql/Mariadb/Postgresql